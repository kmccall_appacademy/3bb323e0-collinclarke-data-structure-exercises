require 'byebug'
# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  sorted_array = arr.sort
  sorted_array[-1] - sorted_array[0]
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr.sort == arr
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  vowels = 'aeiou'
  vowel_count = 0
  characters = str.chars
  characters.each do |ch|
    if vowels.include?(ch.downcase)
      vowel_count += 1
    end
  end
  vowel_count
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  vowels = 'aeiou'
  vowels_caps = vowels.upcase
  no_vowels = str.delete(vowels + vowels_caps)
  no_vowels
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
int_array = int.to_s.split('')
sorted_numbers = int_array.sort
sorted_numbers.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  characters = str.chars
  comparison = characters.dup
  characters.each_with_index do |ch, idx|
   comparison.each_with_index do |ch2, idx2|
     if ch.downcase == ch2.downcase && idx != idx2
       return true
     end
   end
  end
 false
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
area_code = arr[0..2]
first_three = arr[3..5]
second_four = arr[6..9]
"(#{area_code.join}) #{first_three.join}-#{second_four.join}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
 s_int = str.split(',')
 int_arr = []
 s_int.each {|str| int_arr << str.to_i}
 asc_arr = int_arr.sort
 asc_arr[-1] - asc_arr[0]
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!

def my_rotate(arr, offset=1)
  # debugger
 if offset < 0
   return backwards(arr, offset)
 else
   forwards(arr, offset)
 end
end

def forwards(arr, offset=1)
  rotation = 0
  until rotation == offset.abs
   arr = arr.drop(1) + arr.take(1)
   rotation += 1
  end
 arr
end

def backwards(arr, offset=1)
  rotation = 0
  until rotation == offset.abs
   arr = arr.drop(arr.length - 1) + arr.take(arr.length - 1)
   rotation += 1
  end
 arr
end
